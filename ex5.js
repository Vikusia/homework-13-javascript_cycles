//ex6
let mas = [];
let m = 3, n = 3;

for (let i = 0; i < m; i++){
    mas[i] = [];
    for (let j = 0; j < n; j++){
        mas[i][j] = parseFloat(Math.random().toFixed(2));
    }}
console.log(mas);

let sum = 0, srZnach = 0;
for ( let subMas of mas)
    for(let number of subMas){
        sum += number;

    }

srZnach = sum / (m * n);

sum = parseFloat(sum.toFixed(2));
srZnach = parseFloat(srZnach.toFixed(2));

console.log(`Сума елементів масиву = ${sum}`);
console.log(`Середнє арифмитичне масиву = ${srZnach}`);
